# Spektrix Drupal Module #

### What is this repository for? ###

It integrates the ticketing system Spektrix with Drupal.  The current version supports:

* Pulling events and related instances from Spektrix using the v1 API (without authentication)
* Querying and adding customers to Spektrix using the v2 API (with authentication)
* Associating Spektrix iFrames with Drupal paths
* Providing Spektrix iFrames as Drupal block

### Upcoming releases ###

* A full sign up form implementation with configurable preference tags
* Further v2 API functionality

### Who do I talk to? ###

* abhinav@marmelodigital.com
* luke@marmelodigital.com