<?php
/*
TODO:
    - When we fetch event information, calculate a hash of the data.  This can be used to check if an update has occurred on Spektrix side (and provide some clues as to whether we're getting caches details).

*/
function spektrix_feed_url(){
    $client_id = variable_get('spektrix_client_id', NULL);
    if($client_id == NULL) return FALSE;
    return str_replace('{client_id}', $client_id, SPEKTRIX_FEED_BASE_URL);
}

// 6.1 GetAllInstancesFrom
// 6.2 GetAllInstancesFromAllAttributes
function spektrix_get_instances_from($date_string=NULL, $all_attributes = true){
    if($date_string == NULL)
        $date_string = date('Y-m-d').'T'.date('H:i:s');
    return qp(spektrix_feed_url().(($all_attributes) ? 'allattributes/' : '').'from?date='.$date_string, SPEKTRIX_FEED_ROOT);
}

// 6.3 GetAllInstancesFromTo
// 6.4 GetAllInstancesFromToAllAttributes
function spektrix_get_instances_from_to($from_date_string = NULL, $to_date_string = NULL, $all_attributes = true){
    if($from_date_string == NULL) return spektrix_get_instances_from(NULL, $all_attributes);
    if($to_date_string == NULL) return spektrix_get_instances_from($from_date_string, $all_attributes);
    return qp(spektrix_feed_url().'alltimes/'.(($all_attributes) ? 'allattributes/' : '').'?dateFrom='.$from_date_string.'&dateTo='.$to_date_string, SPEKTRIX_FEED_ROOT);
}

// 6.5 GetEvent  
// 6.6 GetEventAllAttributes
function spektrix_get_event_details($event_id = NULL, $all_attributes = true){
    if($event_id == NULL) return FALSE;
    return qp(spektrix_feed_url().'details/'.(($all_attributes) ? 'allattributes/' : '').$event_id, 'Event');
}

// 6.7 GetFrom
// 6.8 GetFromAllAttributes
function spektrix_get_from($date_string=NULL, $all_attributes = true){
    if($date_string == NULL)
        $date_string = date('Y-m-d').'T'.date('H:i:s');
    return qp(spektrix_feed_url().(($all_attributes) ? 'allattributes/' : '').'from?date='.$date_string, SPEKTRIX_FEED_ROOT);
}

// 6.9 GetFromTo
// 6.10 GetFromToAllAttributes
function spektrix_get_from_to($from_date_string = NULL, $to_date_string = NULL, $all_attributes = true){
    if($from_date_string == NULL) return spektrix_get_from(NULL, $all_attributes);
    if($to_date_string == NULL) return spektrix_get_from($from_date_string, $all_attributes);
    return qp(spektrix_feed_url().(($all_attributes) ? 'allattributes/' : '').'?dateFrom='.$from_date_string.'&dateTo='.$to_date_string, SPEKTRIX_FEED_ROOT);
}

// 6.11 GetNext 
// 6.12 GetNextAllAttributes
function spektrix_get_next_events($count=1, $all_attributes = true){
    return qp(spektrix_feed_url().(($all_attributes) ? 'allattributes/' : '').'next?n='.$count, SPEKTRIX_FEED_ROOT);
}

function spektrix_get_first_event($all_attributes = true){
    $events = spektrix_get_next_events(1, $all_attributes);
    return spektrix_get_next_events(1)->branch()->find('>Event');
}

function spektrix_get_attribute_value(&$event_data, $attribute_name, $attribute_type){
    switch($attribute_type){
        case 'default':
            $attribute_node = $event_data->branch()->find('>'.$attribute_name);
            if($attribute_node->length == 1) return $attribute_node->first()->text();
            return FALSE;
        case 'custom':
            foreach($event_data->branch()->find('>Attributes>EventAttribute') as $attribute_node)
            {
                $attribute_name_node = $attribute_node->branch()->find('>Name');
                if($attribute_name_node->text() == $attribute_name){
                    $attribute_value_node = $attribute_node->branch()->find('>Value');
                    return $attribute_value_node->text();
                }
            }
            return FALSE;
    }
}

function spektrix_get_all_current_and_future_events($all_attributes = true){
    $now = date('Y-m-d').'T'.date('H:i:s');
    return spektrix_get_instances_from($now, $all_attributes);
}