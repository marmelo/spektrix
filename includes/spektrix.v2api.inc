<?php


function spektrix_v2api_url(){
    $client_id = variable_get('spektrix_client_id', NULL);
    if($client_id == NULL) return FALSE;
    return str_replace('{client_id}', $client_id, SPEKTRIX_V2API_BASE_URL);
}

function spektrix_v2api_querystring($params = false) {
        
    if(!$api_key = variable_get("spektrix_apikey")) {
        drupal_set_message("API key not set - please configure Spektrix V2 API settings ", "error");    
    }
    
    // parse params as array and add to query string alongside API key
    $qs = "?api_key=".$api_key;
    
    if(!empty($params)) {
        foreach($params as $key => $value) {

            $qs .= "&".$key."=".$value;

        }            
    }
    
    return $qs;
}

function spektrix_v2api_context($return_options = false) {
    
    $modpath = DRUPAL_ROOT."/".drupal_get_path('module', 'spektrix');    

    // creates a (standard PHP) stream context object
    $options = array(
        'http' => array(
            'method' => 'GET',
            'local_cert' => $modpath.'/certs/'.variable_get('spektrix_client_id', NULL).'-spektrix.cert',
            'local_pk' => $modpath.'/certs/'.variable_get('spektrix_client_id', NULL).'-spektrix.key'        
        )
    );
    
    if($return_options) {
        return $options;
    }
    
    
    return stream_context_create($options);
    
}

function spektrix_v2api_query($url, $getvars = false) {

    $option_details = spektrix_v2api_context(true);        
    
    
    $url = spektrix_v2api_url().$url.spektrix_v2api_querystring($getvars);        
    
    // try via curl
    $curl = curl_init();
    $options = array(
        
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSLCERT => $option_details['http']['local_cert'],
        CURLOPT_SSLKEY => $option_details['http']['local_pk'],
        CURLOPT_HEADER => 0            
    );
    
    curl_setopt_array($curl, $options);    
    
    $return = new stdClass();
    $return->response = curl_exec($curl);    
    $return->curl_info = curl_getinfo($curl);    
    $return->responsecode = $return->curl_info['http_code'];
    $return->request = $url;
    $return->curl_options = $options;    
    $return->curl_error = curl_error($curl);    


    return $return;


    
}

function spektrix_v2api_patch($url, $body = false) {
    $option_details = spektrix_v2api_context(true);            
    
    $url = spektrix_v2api_url().$url.spektrix_v2api_querystring();        
    
    // Need to: 
    // * set headers content type application/xml
    // * set post body
    $requestHeaders = array();
    $requestHeaders[] = "Content-Type: application/xml"; 
    
    // try via curl
    $curl = curl_init();
    $options = array(        
        CURLOPT_URL => $url,
        CURLOPT_CUSTOMREQUEST => "PATCH",
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSLCERT => $option_details['http']['local_cert'],
        CURLOPT_SSLKEY => $option_details['http']['local_pk'],
        CURLOPT_HEADER => 0            
    );
            
    curl_setopt_array($curl, $options);    
    
    if(!empty($body)) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $requestHeaders);
    }    
        

    $return = new stdClass();
    $return->response = curl_exec($curl);
    $return->curl_info = curl_getinfo($curl);    
    $return->responsecode = $return->curl_info['http_code'];    
    $return->request = $body;
    $return->curl_options = $options;
    $return->curl_error = curl_error($curl);

    return $return;    
}

function spektrix_v2api_post($url, $postvalue = false) {
    
    $option_details = spektrix_v2api_context(true);            
    
    $url = spektrix_v2api_url().$url.spektrix_v2api_querystring();        
    
    // Need to: 
    // * set headers content type application/xml
    // * set post body
    $requestHeaders = array();
    $requestHeaders[] = "Content-Type: application/xml"; 
    
    // try via curl
    $curl = curl_init();
    $options = array(        
        CURLOPT_URL => $url,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSLCERT => $option_details['http']['local_cert'],
        CURLOPT_SSLKEY => $option_details['http']['local_pk'],
        CURLOPT_HEADER => 0            
    );
            
    curl_setopt_array($curl, $options);    
    
    if(!empty($postvalue)) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postvalue);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $requestHeaders);
    }    
        

    $return = new stdClass();
    $return->response = curl_exec($curl);
    $return->curl_info = curl_getinfo($curl);    
    $return->responsecode = $return->curl_info['http_code'];    
    $return->request = $postvalue;
    $return->curl_options = $options;
    $return->curl_error = curl_error($curl);

    return $return;
    
    
}

// Updates an existing customer in Spektrix
// NB - only updates tags and statements, requires existing customer XML
// from spektrix_get_customer call.
function spektrix_update_customer($customerxml, $tags, $statements) {

    // get customer id from query string
    $qp = qp($customerxml);
    $id = $qp->find('Customer')->attr("id");

    $tagstring = __spektrix_prepare_tags($tags);
    $dpstring = __spektrix_prepare_statements($statements);
    
    $spektrix_string = "<CustomerUpdate><CustomerId>".$id."</CustomerId>".$tagstring.$dpstring."</CustomerUpdate>";
    
    $qp = qp();
    $qp->xml($spektrix_string);
    $qp->wrapAll("<CustomerUpdates></CustomerUpdates>");
    
    $spcall = spektrix_v2api_patch("/customers", $qp->top()->xml());


    return $spcall;
}

function spektrix_get_tags(){

    // cache tags for max 24 hours
    if($optarray = cache_get('spektrix_tags_cache')) {
        return $optarray->data;
    } else {

        $spcall = spektrix_v2api_query("/tags"); 

        if($spcall->responsecode == 200) {
            $qp = qp($spcall->response);
        
            $optarray = array();
            
            foreach($qp->find('Tag') as $child) {
    
                $id = $child->attr('id');
                $name = $child->find('Name')->text();
                $optarray[$id] = $name;
                
            }    
            
            cache_set('spektrix_tags_cache', $optarray, 'cache', time() + 24*60*60);            
            return $optarray;
        } else {

            watchdog("spektrix", "Could not retrieve tags from Spektrix - HTTP response code ".$spcall->responsecode, NULL, WATCHDOG_ERROR);
            return false;
        }

    }
        
}

function spektrix_get_customer($email) {
    
    $response = spektrix_v2api_query("/customers", array('email' => $email));

    return $response;
    
}

function spektrix_get_statements() {
    

    // cache statements for max 24 hours
    if($ids = cache_get('spektrix_statements_cache')) {
        return $ids->data;
    } else {
        
        $spcall = spektrix_v2api_query("/statements"); 

        if($spcall->responsecode == 200) {

            $qp = qp($spcall->response);
        
            $ids = array();    

            foreach($qp->find('Statement') as $child) {
    
                $ids[$child->attr('id')] = $child->text();
                
            }

            cache_set('spektrix_statements_cache', $ids, 'cache', time() + 24*60*60);                        
            return $ids;            

        } else {
            watchdog("spektrix", "Could not retrieve statements from Spektrix - HTTP response code ".$spcall->responsecode, NULL, WATCHDOG_ERROR);
            return false;            
        }


    }
    
}

function __spektrix_prepare_tags($tags) {
    foreach($tags as $tag) {
        
        $tagstring .= "<Tag id='".$tag."' />";
        
    }

    return $tagstring;
}

function __spektrix_prepare_statements($statements) {
    if(count($statements) > 0) {
        
        foreach($statements as $statement_id) {
            
            $dpstring .= "<Statement id='".$statement_id."'/>";
            
        }
        
        $dpstring = "<AgreedStatements>".$dpstring."</AgreedStatements>";
        
    } else {
        $dpstring = "";
    }
    return $dpstring;
}

function spektrix_create_customer($email, $firstname, $lastname, $tags, $statements) {
    
    $tagstring = __spektrix_prepare_tags($tags);
    $dpstring = __spektrix_prepare_statements($statements);
    
    $spektrix_string = "<NewCustomer><Email>".$email."</Email><FirstName>".$firstname."</FirstName><LastName>".$lastname."</LastName>".$tagstring.$dpstring."</NewCustomer>";
    
    $qp = qp();
    $qp->xml($spektrix_string);
    $qp->wrapAll("<NewCustomers></NewCustomers>");
    
    $spcall = spektrix_v2api_post("/customers", $qp->top()->xml());

    return $spcall;
    
}



function spektrix_configure_v2api($form, &$form_state){
    drupal_add_library('system', 'drupal.collapse');

    $form = array();    
    
    $form['spektrix_apiv2'] = array(
        '#type' => 'container'
    ); 
    
    $form['spektrix_apiv2']['apikey'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get("spektrix_apikey", ""),
        '#description' => "Enter the API key - you can find this in the Spektrix admin interface",
        '#title' => "API key"
    );
     
    $form['spektrix_apiv2']['page_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update settings',
    );
    
    return $form;
}

function spektrix_configure_v2api_submit($form, $form_state) {
    
    variable_set("spektrix_apikey", $form_state['input']['apikey']);
    
    drupal_set_message("Settings updated");
    
}