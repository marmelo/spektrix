<?php

define('SPEKTRIX_DEFAULT_ATTRIBUTE_NAMES', serialize(array(
    array('title' => 'Id', 'feed_type' => 'event'),
    array('title' => 'Name', 'feed_type' => 'event'),
    array('title' => 'Description', 'feed_type' => 'event'),
    array('title' => 'Duration', 'feed_type' => 'event'),
    array('title' => 'FirstInstance', 'feed_type' => 'event'),
    array('title' => 'LastInstance', 'feed_type' => 'event'),
    array('title' => 'OnSaleOnWeb', 'feed_type' => 'event'),
    array('title' => 'ImageUrl', 'feed_type' => 'event'),
    array('title' => 'ThumbnailUrl', 'feed_type' => 'event'),
    array('title' => 'Capacity', 'feed_type' => 'instance'),
    array('title' => 'EventInstanceId', 'feed_type' => 'instance'),
    array('title' => 'SeatsAvailable', 'feed_type' => 'instance'),
    array('title' => 'SeatsLocked', 'feed_type' => 'instance'),
    array('title' => 'SeatsReserved', 'feed_type' => 'instance'),
    array('title' => 'SeatsSelected', 'feed_type' => 'instance'),
    array('title' => 'SeatsSold', 'feed_type' => 'instance'),
    array('title' => 'Time', 'feed_type' => 'instance'),
)));

define('SPEKTRIX_FEED_TYPES', serialize(array(
    'event' => 'Event', 
    'instance' => 'Instance'
)));

define('SPEKTRIX_FEED_ROOT', 'Events');

define('SPEKTRIX_FEED_TYPE_SELECTORS', serialize(array(
//    'event' => '>Event', 
    'event' => 'Event', 
    'instance' => 'Event>Times>EventTime',
    'event_instance' => '>Times>EventTime',
    
)));

define('SPEKTRIX_ATTRIBUTE_TYPES', serialize(array('default', 'custom')));

define('SPEKTRIX_BASE_URL', 'https://system.spektrix.com/{client_id}');
define('SPEKTRIX_FEED_BASE_URL', SPEKTRIX_BASE_URL.'/api/v1/eventsrestful.svc/');
define('SPEKTRIX_WEB_BASE_URL', SPEKTRIX_BASE_URL.'/website/');

define('SPEKTRIX_V2API_BASE_URL', 'https://api.system.spektrix.com/{client_id}/api/v2');

define('SPEKTRIX_STATUS_UPDATED',  0);  // Fields have been updated using spektrix feed
define('SPEKTRIX_STATUS_CHANGES',  1);  // Changes were detected from feed but not applied
define('SPEKTRIX_STATUS_UPTODATE', 2);  // Node is already up to date


define('SPEKTRIX_MODULE_NAME', 'Spektrix Integration');