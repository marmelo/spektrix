<?php

function spektrix_import_events(){
    // Retrieve a spektrix feed of future events from current time
    // The feed is returned as a querypath object
    $spektrix_feed = spektrix_get_all_current_and_future_events(true);
    $feed_types = unserialize(SPEKTRIX_FEED_TYPES);
    // Feed selectors tell us the XML tag names for events and instances to be used by querypath
    $feed_selectors = unserialize(SPEKTRIX_FEED_TYPE_SELECTORS);
    // Get Event content type
    $event_node_type = variable_get('spektrix_event_node_type', NULL);
    if($event_node_type == NULL){
        //watchdog(SPEKTRIX_MODULE_NAME, 'Import failed.  Spektrix Event node type not set', array(), WATCHDOG_ERROR);
        return;
    }
    $instance_node_type = variable_get('spektrix_instance_node_type', NULL);
    if($instance_node_type == NULL){
        //watchdog(SPEKTRIX_MODULE_NAME, 'Import failed.  Spektrix Instance node type not set', array(), WATCHDOG_ERROR);
        return;
    }
    // Get default event attribute fields.  We need this to ensure that the Id attribute has
    // been mapped to a field
    $spektrix_event_default_attribute_fields = variable_get('spektrix_event_default_attribute_fields', NULL);
    if($spektrix_event_default_attribute_fields == NULL || !array_key_exists('Id', $spektrix_event_default_attribute_fields))
    {
        //watchdog(SPEKTRIX_MODULE_NAME, 'Import failed.  You must define a field for the event Spektrix ID attribute', array(), WATCHDOG_ERROR);
        return;
    }
    // Get default instance attribute fields.  We need this to ensure that the Id attribute has
    // been mapped to a field
    $spektrix_instance_default_attribute_fields = variable_get('spektrix_instance_default_attribute_fields', NULL);
    if($spektrix_instance_default_attribute_fields == NULL || !array_key_exists('EventInstanceId', $spektrix_instance_default_attribute_fields))
    {
        //watchdog(SPEKTRIX_MODULE_NAME, 'Import failed.  You must define a field for the instance Spektrix ID attribute', array(), WATCHDOG_ERROR);
        return;
    }
    // Loop through each Event returned in the feed
    foreach($spektrix_events = $spektrix_feed->branch()->find($feed_selectors['event']) as $spektrix_event){
        // Check if a node exists for this event by checking the Spektrix ID field
        $event_result = spektrix_check_node_exists(spektrix_get_attribute_value($spektrix_event, 'Id', 'default'), $spektrix_event_default_attribute_fields['Id'], $event_node_type);
        // If the node does not exist create it
        if(!$event_result){
            // Create node from feed data and get node ID out.  Set Watchdog message.
            $event_node_id = spektrix_create_node($spektrix_event, 'event');
            $event_message = '%node_title: event created.';
            $event_message_level = WATCHDOG_INFO;
        }
        else{
            // Check if node has been updated in the feed and set watchdog message.
            $event_node_id = array_keys($event_result['node'])[0];
            if(spektrix_update_node($event_node_id, $spektrix_event, 'event')){
                $event_message = '%node_title: event has been updated on Spektrix.';
                $event_message_level = WATCHDOG_NOTICE;
            }
            else{
                $event_message = '%node_title: event already exists and is up to date.';
                $event_message_level = WATCHDOG_INFO;
            }
        }
        $event_node = node_load($event_node_id);    // Load node object
        //watchdog(SPEKTRIX_MODULE_NAME, $event_message, array('%node_title' => $event_node->title), $event_message_level);
       
        $instance_node_ids = array();
        foreach($spektrix_instances = $spektrix_event->branch()->find($feed_selectors['event_instance']) as $spektrix_instance){
            if($instance_id = spektrix_get_attribute_value($spektrix_instance, 'EventInstanceId', 'default')){
                $instance_result = spektrix_check_node_exists($instance_id, $spektrix_instance_default_attribute_fields['EventInstanceId'], $instance_node_type);
                if(!$instance_result){
                    $instance_node_id = spektrix_create_node($spektrix_instance, 'instance', $event_node_id);
                    $instance_message = '%event_node_title - %instance_node_title: instance created.';
                    $instance_message_level = WATCHDOG_INFO;
                }
                else{
                    $instance_node_id = array_keys($instance_result['node'])[0];
                    if(spektrix_update_node($instance_node_id, $spektrix_instance, 'instance')){
                        $instance_message = '%event_node_title - %instance_node_title: instance has been updated on Spektrix.';
                        $instance_message_level = WATCHDOG_NOTICE;
                    }
                    else{
                        $instance_message = '%event_node_title - %instance_node_title: instance is already up to date.';
                        $instance_message_level = WATCHDOG_INFO;
                    }
                }
                $instance_node = node_load($instance_node_id);
                //watchdog(SPEKTRIX_MODULE_NAME, $instance_message, array('%event_node_title' => $event_node->title, '%instance_node_title' => $instance_node->title), $instance_message_level);
                $instance_node_ids[] = $instance_node_id;
            }
            
        }
        $lost_instances = spektrix_get_non_feed_instances($event_node_id, $instance_node_type, $instance_node_ids);
        foreach($lost_instances as $lost_instance)
            //watchdog(SPEKTRIX_MODULE_NAME, 'Instance for event %event_node_title - %lost_instance has been removed from spektrix', array('%event_node_title' => $event_node->title, '%lost_instance' => $lost_instance), WATCHDOG_WARNING); 
        spektrix_attach_orphaned_instances();
        
    }
}

function spektrix_attach_orphaned_instances(){
    $instance_linking_field = variable_get('spektrix_instance_linking_field', NULL);
    $event_linking_field = variable_get('spektrix_event_linking_field', NULL);
    $instance_node_type = variable_get('spektrix_instance_node_type', NULL);
    if ($instance_linking_field == NULL || $event_linking_field == NULL || $instance_node_type == NULL) return array();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
         ->entityCondition('bundle', $instance_node_type);
    $result = $query->execute();
    if(!$result) return array();
    foreach($result['node'] as $node_id => $entity){
        $entity_data = entity_load('node', array($node_id));
        if(!isset($entity_data[$node_id]->{$event_linking_field}[LANGUAGE_NONE][0]['target_id'])) continue;
        $event_id = $entity_data[$node_id]->{$event_linking_field}[LANGUAGE_NONE][0]['target_id'];
        $event_entity = entity_load_single('node',$event_id);
        $instance_found = FALSE;
        if(isset($event_entity->{$event_linking_field}[LANGUAGE_NONE])){
            foreach($event_entity->{$event_linking_field}[LANGUAGE_NONE] as $performance){
                if($performance['target_id'] == $node_id){
                    $instance_found = TRUE;
                    break;
                }
            }
        }
        if(!$instance_found){
            $event_entity->{$event_linking_field}[LANGUAGE_NONE][] = array('target_id' => $node_id);
            entity_save('node', $event_entity);
        }
    }
}

function spektrix_get_non_feed_instances($event_node_id, $instance_node_type, $instance_node_ids){
    $instance_linking_field = variable_get('spektrix_instance_linking_field', NULL);
    if ($instance_linking_field == NULL) return array();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
         ->entityCondition('bundle', $instance_node_type)
         ->entityCondition('entity_id', $instance_node_ids, 'NOT IN')
         ->fieldCondition($instance_linking_field, 'target_id' ,  $event_node_id, '=');
    $result = $query->execute();
    if(!$result) return array();
    $return_array = array();
    foreach($result['node'] as $node_id => $entity){
        $entity_data = entity_load('node', array($node_id));
        $return_array[] = $entity_data[$node_id]->title.' ('.$node_id.')';
    }
    return $return_array;
}

function spektrix_check_node_exists($spektrix_id, $spektrix_id_field, $node_type){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', $node_type)
          ->fieldCondition($spektrix_id_field['field'] ,$spektrix_id_field['column'], $spektrix_id);
    return $query->execute();
}

function spektrix_update_node($node_id, $feed_data, $feed_type){
    $node_type = variable_get('spektrix_'.$feed_type.'_node_type', NULL);
    if($node_type == NULL){
        //watchdog(SPEKTRIX_MODULE_NAME, 'Node update failed (%nodeid).  Spektrix '.$feed_type.' node type not set', array('%nodeid' => $node_id), WATCHDOG_ERROR);
        return;
    }
    
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', $node_type)
          ->entityCondition('entity_id', $node_id);
    $result = $query->execute();

    if(isset($result['node'][$node_id])){
        $entity = entity_load_single('node',$node_id);
        $entity_wrapper = entity_metadata_wrapper('node', $entity);
        $attribute_types = unserialize(SPEKTRIX_ATTRIBUTE_TYPES);
        $comparison_array = array();
        foreach($attribute_types as $attribute_type){
            $fields = variable_get('spektrix_'.$feed_type.'_'.$attribute_type.'_attribute_fields', NULL);
            foreach($fields as $attribute_name => $field_data){
                if(($feed_value = spektrix_get_attribute_value($feed_data, $attribute_name, $attribute_type)) !== FALSE){
                    if(isset($entity_wrapper->{$field_data['field']})){
                        $field_info = field_info_field($field_data['field']);
                        $field_label = field_info_instance('node', $field_data['field'], $node_type)['label'];
                        
                        if($field_data['field'] == 'title'){ 
                            $current_values = $entity->title;
                            $field_label = 'Title';
                        }
                        else $current_values = field_get_items('node', $entity, $field_data['field']);
                        if(!isset($comparison_array[$field_data['field']]))
                           $comparison_array[$field_data['field']] = array('current_value' => $current_values, 'autoupdate' => $field_data['autoupdate'], 'field_label' => $field_label);
                        
                        switch($field_info['type']){
                            case 'entityreference':
                                if(!empty($feed_value)){
                                    if($field_info['settings']['target_type'] == 'node'){
                                        $bundles = array_keys($field_info['settings']['handler_settings']['target_bundles']);
                                        $query = new EntityFieldQuery();
                                        $query->entityCondition('entity_type', 'node')
                                            ->entityCondition('bundle', $bundles, 'IN')
                                            ->propertyCondition('title', $feed_value);
                                        $result = $query->execute();
                                        if($result){
                                            $matched_nodes = array_keys($result['node']);
                                            if(count($matched_nodes) > 1){
                                                
                                                //watchdog(SPEKTRIX_MODULE_NAME, 'Field update failed.  Multiple matches found for field %field_name with value %feed_value', array('%field_name' => $field_data['field'], '%feed_value' => $feed_value), WATCHDOG_WARNING);
                                            }
                                            else
                                                $comparison_array[$field_data['field']]['feed_value'][] = array($field_data['column'] => $matched_nodes[0]);
                                        }
                                        else{
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field update failed.  Entity reference not found for %field_name', array('%field_name' => $field_data['field']), WATCHDOG_WARNING);
                                        }
                                    }
                                }
                                break;
                            case 'taxonomy_term_reference':
                                if(isset($field_info['settings']['allowed_values'][0]['vocabulary'])){
                                    if(!empty($feed_value))
                                    {
                                        $taxonomy_term = taxonomy_get_term_by_name($feed_value, $field_info['settings']['allowed_values'][0]['vocabulary']);

                                        if(count($taxonomy_term) == 0){
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field update failed.  Taxonomy term not found for %field_name', array('%field_name' => $field_data['field']), WATCHDOG_WARNING);
                                        }
                                        elseif(count($taxonomy_term) > 1){
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field update failed.  Multiple matches found for field %field_name with value %feed_value', array('%field_name' => $field_data['field'], '%feed_value' => $feed_value), WATCHDOG_WARNING);
                                        }
                                        else
                                            $comparison_array[$field_data['field']]['feed_value'][] = array($field_data['column'] => intval(array_keys($taxonomy_term)[0]));
                                    }
                                }
                                break;
                            default:
                                if($feed_value == 'true') $feed_value = 1;
                                if($feed_value == 'false') $feed_value = 0; 
                                if($field_data['field'] == 'title')
                                    $comparison_array[$field_data['field']]['feed_value'] = $feed_value;
                                else{    
                                    //if(isset($comparison_array[$field_data['field']]['feed_value'][0]))
                                    if(!isset($comparison_array[$field_data['field']]['feed_value'][0]))
                                        $comparison_array[$field_data['field']]['feed_value'][0] = $comparison_array[$field_data['field']]['current_value'][0];
                                    $comparison_array[$field_data['field']]['feed_value'][0][$field_data['column']] = spektrix_clean_feed_values($field_info['type'],$feed_value);
                                }
                                
                                break;
                        }
                        if(!isset($comparison_array[$field_data['field']]['feed_value']))
                            $comparison_array[$field_data['field']]['feed_value'] = array();
                    }
                }
            }
        }
        $changed_fields = ", ";
        $updated_fields = ", ";
        $uptodate_fields = ", ";
        $changes = false;
        $updates = false;
    
        
        
        foreach($comparison_array as $field_name => $comparison_info){
            if(isset($comparison_info['current_value']) && isset($comparison_info['feed_value']) && $comparison_info['current_value'] == $comparison_info['feed_value'])
              $uptodate_fields .= $comparison_info['field_label'].', ';
            else{
                $changes = true;
                if($comparison_info['autoupdate'] === 1){
                    $updates = true;
                    $entity->{$field_name}[LANGUAGE_NONE] = $comparison_info['feed_value'];
                    $updated_fields .= $comparison_info['field_label'].', ';
                }
                else
                    $changed_fields .= $comparison_info['field_label'].', ';
            }
        }
        if($updates) entity_metadata_wrapper('node', $entity)->save();
        $changed_fields = substr($changed_fields, 2, -2).' ';
        $updated_fields = substr($updated_fields, 2, -2).' ';
        $uptodate_fields = substr($uptodate_fields, 2, -2).' ';
        if($changed_fields == ' ') $changed_fields = "No fields ";
        if($updated_fields == ' ') $updated_fields = "No fields ";
        if($uptodate_fields == ' ') $uptodate_fields = "No fields ";
        
        if($changes){
            
            //watchdog(SPEKTRIX_MODULE_NAME, 'Changes were detected for the %feedtype %entitytitle.  %updated_fields have been autoupdated.  %changed_fields have changed but the changes were not imported.  %uptodate_fields were all up to date.', array('%feedtype' => $feed_type,'%entitytitle' => $entity->title,'%updated_fields' => $updated_fields,'%changed_fields' => $changed_fields,'%uptodate_fields' => $uptodate_fields,), WATCHDOG_NOTICE);
        }
        else{
            //watchdog(SPEKTRIX_MODULE_NAME, 'No changes were detected for the %feedtype %eventtitle', array('%feedtype' => $feed_type, '%eventtitle' => $entity->title), WATCHDOG_INFO);
        }
        
        return $changes;
    }
    return false;

}

function spektrix_create_node($feed_data, $feed_type, $event_id=NULL){
    global $user;
    
    $node_type = variable_get('spektrix_'.$feed_type.'_node_type', NULL);
    if($node_type == NULL){
        //watchdog(SPEKTRIX_MODULE_NAME, 'Node creation failed.  Spektrix '.$feed_type.' node type not set', array(), WATCHDOG_ERROR);
        return;
    }
    
    $values = array(
      'type' => $node_type,
      'uid' => $user->uid,
      'status' => NODE_NOT_PUBLISHED,
      'comment' => 0,
      'promote' => NODE_NOT_PROMOTED,
    );
    $entity = entity_create('node', $values);    
    $entity_wrapper = entity_metadata_wrapper('node', $entity);

    $attribute_types = unserialize(SPEKTRIX_ATTRIBUTE_TYPES);
    foreach($attribute_types as $attribute_type){
        $fields = variable_get('spektrix_'.$feed_type.'_'.$attribute_type.'_attribute_fields', NULL);
        if($feed_type == 'instance')
            if(isset($fields['Time']))
                if($title = spektrix_get_attribute_value($feed_data, 'Time', 'default'))
                    $entity_wrapper->title->set(date_create_from_format('Y-m-d H:i:s', str_replace('T', ' ',$title))->format('l jS M Y H:i'));
        foreach($fields as $attribute_name => $field_data){
            if(($feed_value = spektrix_get_attribute_value($feed_data, $attribute_name, $attribute_type)) !== FALSE){
                if(!empty($field_data['field']) && !empty($field_data['column'])){
                    if(isset($entity_wrapper->{$field_data['field']})){
                        $field_info = field_info_field($field_data['field']);
                        
                        $current_values = field_get_items('node', $entity, $field_data['field']);
                        
                        switch($field_info['type']){
                            case 'entityreference':
                                if(!empty($feed_value)){
                                    if($field_info['settings']['target_type'] == 'node'){
                                        $bundles = array_keys($field_info['settings']['handler_settings']['target_bundles']);
                                        $query = new EntityFieldQuery();
                                        $query->entityCondition('entity_type', 'node')
                                            ->entityCondition('bundle', $bundles, 'IN')
                                            ->propertyCondition('title', $feed_value);
                                        $result = $query->execute();
                                        if($result){
                                            $matched_nodes = array_keys($result['node']);
                                            if(count($matched_nodes) > 1){
                                                //watchdog(SPEKTRIX_MODULE_NAME, 'Field import failed.  Multiple matches found for field %field_name with value %feed_value', array('field_name' => $field_data['field'], 'feed_value' => $feed_value), WATCHDOG_WARNING);
                                            }
                                            else{
                                                if($field_info['cardinality'] == 1){
                                                    $entity_wrapper->{$field_data['field']}->set(intval($matched_nodes[0]));
                                                }
                                                else{
                                                    $entity_wrapper->{$field_data['field']}[] = intval($matched_nodes[0]);
                                                }
                                            }
                                        }
                                        else{
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field import failed.  Entity reference not found for %field_name', array('field_name' => $field_data['field']), WATCHDOG_WARNING);
                                        }
                                    }
                                }
                                break;
                            case 'taxonomy_term_reference':
                                if(!empty($feed_data)){
                                    if(isset($field_info['settings']['allowed_values'][0]['vocabulary'])){
                                        $taxonomy_term = taxonomy_get_term_by_name($feed_value, $field_info['settings']['allowed_values'][0]['vocabulary']);

                                        if(count($taxonomy_term) == 0){
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field import failed.  Taxonomy term not found for %field_name', array('field_name' => $field_data['field']), WATCHDOG_WARNING);
                                        }
                                        elseif(count($taxonomy_term) > 1){
                                            //watchdog(SPEKTRIX_MODULE_NAME, 'Field import failed.  Multiple matches found for field %field_name with value %feed_value', array('field_name' => $field_data['field'], 'feed_value' => $feed_value), WATCHDOG_WARNING);
                                        }
                                        else{
                                            if($field_info['cardinality'] == 1){
                                                $entity_wrapper->{$field_data['field']}->set(intval(array_keys($taxonomy_term)[0]));
                                            }
                                            else{
                                                $entity_wrapper->{$field_data['field']}[] = intval(array_keys($taxonomy_term)[0]);
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                
                                if($feed_value == 'true') $feed_value = 1;
                                if($feed_value == 'false') $feed_value = 0; 

                                if($field_data['field'] == 'title')
                                    $entity_wrapper->title->set($feed_value);
                                else{
                                    $field_name = $field_data['field'];
                                    //$current_value = $entity->{$field_data['field']}[LANGUAGE_NONE][0];
                                    $current_value = $entity->$field_name[LANGUAGE_NONE][0];
                                    if(empty($current_value)) $current_value = array();
                                    // $entity->{$field_data['field']}[LANGUAGE_NONE][0] = array_merge($current_value, array(
                                    $entity->$field_name[LANGUAGE_NONE][0] = array_merge($current_value, array(
                                        $field_data['column'] =>  spektrix_clean_feed_values($field_info['type'],$feed_value),
                                    ));
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
    
    $result = FALSE;
    if($feed_type == 'instance'){
        $spektrix_instance_linking_field = variable_get('spektrix_instance_linking_field', NULL);
    
        $query = new EntityFieldQuery();

        $query->entityCondition('entity_type', 'node')
            ->entityCondition('entity_id', $event_id)
            ->entityCondition('bundle', variable_get('spektrix_event_node_type', NULL));
        $result = $query->execute();

        
        if(field_info_field($spektrix_instance_linking_field) && $result){
            $entity_wrapper->{$spektrix_instance_linking_field}->set(intval($event_id));
            
        }
    }
    $entity_wrapper->save();
    if($result){
        $spektrix_event_linking_field = variable_get('spektrix_event_linking_field', NULL);
        
        if(field_info_field($spektrix_event_linking_field)){
            $event_entity = entity_load('node',array_keys($result['node']));    
            $event_entity_wrapper = entity_metadata_wrapper('node', $event_entity[$event_id]);      
            $event_entity_wrapper->{$spektrix_event_linking_field}[] = $entity->nid;
            $event_entity_wrapper->save();
        }
        
    }
    return $entity->nid;
}

function spektrix_clean_feed_values($type, $feed_value){
    switch($type){
        case 'list_boolean':
            return array_search($feed_value, array(TRUE, 'true', 1, '1' )) !== FALSE ? 1 : 0;
        case 'datetime':
            return str_replace('T', ' ',$feed_value);
    }
    return $feed_value;
}