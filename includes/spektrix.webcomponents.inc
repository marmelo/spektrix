<?php


function spektrix_configure_webcomponents($form, &$form_state){
    drupal_add_library('system', 'drupal.collapse');

    $form = array();    
    
    $form['spektrix_webcomponents'] = array(
        '#type' => 'container'
    ); 
    
    $form['spektrix_webcomponents']['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => variable_get("spektrix_webcomponents_enabled", 0),
        '#description' => "Should widgets be enabled on this site or not?",
        '#title' => "Widgets enabled?"
    );
     
    $form['spektrix_webcomponents']['page_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update settings',
    );
    
    return $form;
}

function spektrix_configure_webcomponents_submit($form, $form_state) {
    
    variable_set("spektrix_webcomponents_enabled", $form_state['input']['enabled']);
    
    drupal_set_message("Settings updated");
    
}