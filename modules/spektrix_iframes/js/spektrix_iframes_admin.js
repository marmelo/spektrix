(function ($, Drupal, window, document, undefined) {
    Drupal.behaviors.spektrix_iframes_form = {
        attach: function(context, settings) {
            console.log('HERE');
            $(document).ready(function() { 
                $('#edit-spektrix-page-list').change(function(event) {
                    window.location = '/admin/config/services/spektrix/iframes/' + $(event.target).val();
                });
            });
        }
    };
})(jQuery, Drupal, this, this.document);