(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.spektrix_iframes_setup = {

    attach: function(context, settings) {
        $('iframe').on('load', function() {  
            if($('#page-title').length == 1)
                position = $('#page-title').offset();
            else
                position = $(this).offset();
            if($(window).scrollTop() > position.top)
                window.parent.parent.scrollTo(position.left, position.top);
        });
        
        $(document).ready(function() {
            
            if($(window).width() <= 480){
                $('iframe').prop('src', $('iframe').prop('src') + '&Optimise=mobile');
                
            }
            
            $('.spektrix-frame-skip').click(scrollToFrame);
                
        });
        
        function scrollToFrame(){
            $('html, body').animate({ scrollTop: $('#SpektrixIFrame').offset().top - 150}, 2000);
        }
        
    }
};

})(jQuery, Drupal, this, this.document);