(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.spektrix_attributes_autoupdate_link = {
    attach: function(context, settings) {
        $('input[type=checkbox]').click(function(){
            var checkedValue = $(this).prop('checked');
            $('input[value="' + $(this).parent().parent().parent().find('input[type=hidden].field_label').val() + '"]').each(function() {
                $('input[name="' + $(this).prop('name').replace('][label]', '][autoupdate]') + '"]').prop('checked', checkedValue); 
            })
        });

    }
}

})(jQuery, Drupal, this, this.document);